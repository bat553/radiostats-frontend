<html>
<?php 
/*
///////////////////////////////
		|Swlabe Project|
//////////////////////////////

Ce fichier permet de choisir entre spotify et deezer
*/
?>
<title>Select Service.</title>
<link rel="stylesheet" href="./index.css" >
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<div class="wrap">
  <a href="./index.php?service=spotify" class="button">Spotify</a>
  <a  href="./index.php?service=deezer" class="button2">Deezer</a>
</div>

</html>

<?php
session_start();
error_reporting(E_ALL);
if (isset($_GET['service'])){
	$service = htmlspecialchars($_GET['service']);
	if ($service == 'spotify') { header('Location: ./spotify.php' );}
	if ($service == 'deezer') { header('Location: ./deezer.php' ); }
	else { echo 'Error 400';}
}
?>
<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<!-- Piwik Tracker-->
<script src="../include/js/stat.js" ></script>
<img src="https://stats.swano-lab.net/js/?idsite=12&rec=1" style="border:0" alt="" />
<!-- End Piwik -->
