<?php
/*
///////////////////////////////
		|Swlabe Project|
//////////////////////////////

Ajout de la session avec le token pour le oauth2
*/

error_reporting(-1);
ini_set('display_errors', 1);
require 'vendor/autoload.php';
$session = new SpotifyWebAPI\Session('2e3f51b377f14a6da4c2e2eb3fff4fc1', '3ed4f236d2bc47059e03038a241ae94f', 'http://swla.be/oauth/callback.php');
$scopes = [
    'scope' => [
    'playlist-read-private',
    'user-read-private',
	'playlist-modify-public',
	'user-follow-read',
	'user-read-email',
	'user-follow-modify',
	'user-library-modify',
	'user-library-read'

        ],
    ];
$authorizeUrl = $session->getAuthorizeUrl(array(
    'scope' => $scopes
));
$api = new SpotifyWebAPI\SpotifyWebAPI();
session_start();
if (isset($_SESSION['token'])) { 
    $api = new SpotifyWebAPI\SpotifyWebAPI();
    $accessToken = $_SESSION['token'];
	$_SESSION['type'] = 'spotify';
    $api->setAccessToken($accessToken);
	header('Location: '. $_SESSION['url'] );
	echo $accessToken;
    
} else {
    header('Location: ' . $session->getAuthorizeUrl($scopes));
    die();
} ?>