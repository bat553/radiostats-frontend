<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 21/02/2018
 * Time: 21:01
 */

if(!$morceau_id) {
    print('Error !');
    http_response_code(500);
    exit(500);

}
$ids = GetMorceauID($morceau_id);
$title = $ids['morceau'].' - '.$ids['artiste'].' | RadioStats v1.2';
SetTitle($title);
$img = json_decode(json_encode(GetTrackDeezer($ids['DeezerId'])), True);
$class = GetMorceauTop5000();
$class = json_decode($class, true);
$classement = array_search($morceau_id, array_column($class, 'key'));
//print_r($classement);
$array_classement = array();

if(is_int($classement)){
    $found = True;
    $note = GetNote($class, $classement);



    array_push($array_classement, $morceau_id); // Affichage classement
        //Chercher deriere
        $a = $classement;
        $a = $a + 3; // pas prendre celui en cours
        while ($a > $classement) {
            array_push($array_classement, $class[$a]['key']);
            --$a;

        }
        $a = $classement;
        $a = $a - 3;
        while ($a < $classement) {
            array_push($array_classement, $class[$a]['key']);
            ++$a;
        }

    //print_r($array_classement);
}
else{
    $found = False;
    array_push($array_classement, 'notfound');
    $note = array("Non noté", "#625a5a");

}

?>

<body id="page-top">
    <header class=" bg-primary text-white text-center">
	<div class="container" style='padding-top : 15%; '>
	  <div class="row justify-content-md-center"  style="padding-bottom:5%">
			<div class='col col-sm-4 dark'>
				<a href="./infodex.php?artiste_id=<?=$ids['artiste_id']?>" class='title dark-link'><h3><?=$ids['artiste'] ?></h3></a>
				<p class= 'title'><h5><?=$ids['morceau'] ?></h5></p>
				<br></br>
				<p class= 'title'><h3>Note : </h3> <h2 style="color:<?=$note[1]?> "><?=$note[0]?></h2></p>
			</div>
			<div class='col col-sm-6 dark'>
				<img src="<?=$img['album']['cover_big'] ?>" width="350" height="330" alt="<?=$ids['morceau'] ?>"/>
			</div>
		</div>
	</div>
    </header>
<section>
	<div class="container">
	<div class="row">
	<div class="col-sm-7" style="text-align:center; ">
		<p class="title"> Top diffuseurs</p>
		<div style="overflow:auto; height: 250px; width: 100%; overflow-x: hidden;">
			<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Radio</th>
							<th scope="col">Joué</th>
						</tr>
					</thead>
					<tbody>
                    <?php

                    if (GetMorceauRadio($morceau_id) != NULL){
                        $radio_array = GetMorceauRadio($morceau_id);
                        $a = 0;
                        while ($a < 20 && $radio_array[$a] != NULL){
                            $radio = strtoupper($radio_array[$a]['key']);
                            $total = $radio_array[$a]['doc_count'];
                            ++$a;

                            echo "<tr><th>".$a."</th><th><a href='./radiodex.php?radio=".$radio."'>". $radio ."</a></th><th>".$total."</th></tr>";

                        }
                    }

                    else { echo "<tr>Le classement des tendances n'est pas encore opérationnel. Revenez dans quelques heures.</tr>";}
                    ?>
					</tbody>
				</table>
		</div>
	</div>
	<div class="col-sm-1"></div>
	<div class="col-sm-3" >
			<div class="current-play title" style="padding-bottom:15%">
    Classement
			</div>
        <?php
        if ($found == true) {
            require_once 'morceau-class.php';
        } else {
            print '<div class="show-current dark">';
            print('Ce morceau ne figure pas dans le Top 5000...');
            print '</div>';
        }
        ?>
	</div>
</div>
</div>
    <div class="full-width container" style=" padding-top: 5%">
        <div class="ct-chart"></div>
        <script>
            var jsonData = '<?php print GetPlayTime('morceau', $morceau_id); ?>';
            var data = JSON.parse(jsonData);
            new Chartist.Line('.ct-chart', data, {divisor: 1, low:0, area:true});
        </script>
    </div>
</div>
</section>