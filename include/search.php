<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 21/02/2018
 * Time: 20:52
 */

SetTitle('Recherche "'.$_GET['q'].'" | RadioStats v.1.2');
print (' <form action="searchdex.php" class="form-inline my-2 my-lg-0" method="get">
            <input class="form-control input-lg" style="width: 100%; margin-bottom: 15px"" name="q" placeholder="Search.." type="search">
        </form>');
echo "<table class='table table-striped'>";
if ($_GET['q'] != NULL)
{

    $query = htmlspecialchars($_GET['q']);
    global $client;
    $json = '{
	  "from" : 0, 
	  "size" : 10,
	  "query": {
		"match": {
		  "string": {
			"query": "'.$query.'",
			"operator" : "and",
			"fuzziness": 2  
		  }
		}
	  },
	  "sort": [
		{
			"_score": {
				"order": "desc"
			}
		}
	]
	}';

    $params = [
        'index' => 'radio-link',
        'type' => '_doc',
        'body' => $json
    ];
    $results = $client->search($params);

    if (!$results['hits']['total']) echo "<th>Il n'y a pas encore de données. Revenez plus tard.</th>";
    else {
        echo "<tr><th></th><th></th><th class='text-right'>".$results['hits']['total']." résultat(s) trouvé(s) en ".$results['took']."ms </th></tr>";
        echo "<tr><th>Artiste</th><th>Morceau</th><th>Score</th></tr>";

        if(($results['hits']['total'] < 10) && ($results['hits']['total'] > 1)) {
            $b = $results['hits']['total'];
        }
        else if($results['hits']['total'] == 1){
            $b = 1;
        }
        else {
            $b = 10;
        }
        $a = 0;
        while($b > 0){
            $artiste = $results['hits']['hits'][$a]['_source']['artiste'];
            $morceau = $results['hits']['hits'][$a]['_source']['morceau'];
            $morceau_id = $results['hits']['hits'][$a]['_source']['morceau_id'];
            $artiste_id = $results['hits']['hits'][$a]['_source']['artiste_id'];
            $score = $results['hits']['hits'][$a]['_score'];
            echo "<tr><th><a href='./infodex.php?artiste_id=".$artiste_id."'>". $artiste ."</a></th><th><a href='./infodex.php?morceau_id=".$morceau_id."'>". $morceau ."</a></th><th>".$score."</th></tr>";
            --$b;
            ++$a;
        }

    }

    echo "</table>";
}