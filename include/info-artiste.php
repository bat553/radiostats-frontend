<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 21/02/2018
 * Time: 21:01
 */

if(!$artiste_id) {
    print('Error !');
    http_response_code(500);
    exit(500);

}
$ids = GetArtisteID($artiste_id);
$title = $ids['artiste'].' | RadioStats v1.2';
SetTitle($title);
$img = json_decode(json_encode(GetArtistDeezer($ids['DeezerArtId'])), True);
$spotifyInfos = GetArtistSpotify($ids['SpotifyArtId']);
$class = GetArtistTop5000();
$class = json_decode($class, true);
$classement = array_search($artiste_id, array_column($class, 'key'));
//print_r($classement);
$array_classement = array();

if(is_int($classement)){
    $found = True;
    GetNote($class, $classement);



    array_push($array_classement, $artiste_id); // Affichage classement
    //Chercher deriere
    $a = $classement;
    $a = $a + 3; // pas prendre celui en cours
    while ($a > $classement) {
        array_push($array_classement, $class[$a]['key']);
        --$a;

    }
    $a = $classement;
    $a = $a - 3;
    while ($a < $classement) {
        array_push($array_classement, $class[$a]['key']);
        ++$a;
    }
}


else{
    $found = False;
    array_push($array_classement, 'notfound');
    $note = array("Non noté", "#625a5a");

}

$diffuseurs = GetArtistRadio($artiste_id);
$toptracks = GetArtistTopTracks($artiste_id);

?>
  <body id="page-top">
  <header class=" bg-primary text-white text-center">
	<div class="container dark" style='padding-top : 15%; '>
	  <div class="row justify-content-md-center" style="padding-bottom:5%">
			<div class='col col-sm-4 dark'>
				<p class= 'title'><h3><?=$ids['artiste'] ?></h3></p>
				<p class= 'title'><h5><?php print(strtoupper($spotifyInfos[0]['genres'][0]).' '.strtoupper($spotifyInfos[0]['genres'][1])) ?></h5></p>
				<br></br>
				<p class= 'title'><h3>Note : </h3> <h2 style="color:<?=$note[1]?>"><?=$note[0]?></h2></p>

			</div>
			<div class='col col-sm-6 dark' >
				<img src="<?=$img['picture_big'] ?>" width="350" height="330" alt="<?=$ids['artiste']?>"/>
			</div>
		</div>
	</div>
    </header>
<section>
	<div class="container" style="text-align:center; ">
	<div class="row">
	<div class="col-sm-6" style="text-align:center; ">
		<p class="title"> Top diffuseurs</p>
		<div style="overflow:auto; height: 250px; width: 100%; overflow-x: hidden;">
			<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Radio</th>
							<th scope="col">Joué</th>
						</tr>
					</thead>
					<tbody>
                    <?php
                    if ($diffuseurs != NULL){
                        $a = 0;
                        while ($a < count($diffuseurs)){
                            if (isset($diffuseurs[$a])) {
                                $radio = $diffuseurs[$a]['key'];
                                $infos = GetRadioInfos($radio);
                                $play = $diffuseurs[$a]['doc_count'];
                                ++$a;

                                echo "<tr><th>" . $a . "</th><th><a href='./radiodex.php?radio=" . $radio . "'>" . strtoupper($radio) . "</a></th><th>" . $play . "</th></tr>";
                            }
                            else{
                                break;
                            }
                        }
                    }

                    else { echo "<tr>Le classement des tendances n'est pas encore opérationnel. Revenez dans quelques heures.</tr>";}
                    ?>
					</tbody>
				</table>
		</div>
	</div>
		<div class="col-sm-6" style="text-align:center; ">
		<p class="title"> Top tracks</p>
		<div style="overflow:auto; height: 250px; width: 100%; overflow-x: hidden;">
			<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">Morceau</th>
							<th scope="col">Joué</th>
						</tr>
					</thead>
					<tbody>
                    <?php
                    if ($toptracks != NULL){
                        $a = 0;
                        while ($a < count($toptracks)) {
                            if (isset($toptracks[$a])) {
                                $morceau_id = $toptracks[$a]['key'];
                                $infos = GetMorceauID($morceau_id);
                                $morceau = $infos['morceau'];
                                $play = $toptracks[$a]['doc_count'];
                                ++$a;

                                echo "<tr><th>" . $a . "</th><th><a href='./infodex.php?morceau_id=" . $morceau_id . "'>" . strtoupper($morceau) . "</a></th><th>" . $play . "</th></tr>";
                            }
                            else{
                                break;
                            }
                        }
                    }

                    else { echo "<tr>Le classement des tendances n'est pas encore opérationnel. Revenez dans quelques heures.</tr>";}
                    ?>
					</tbody>
				</table>
		</div>
	</div>
	</div>
        <div class="col-sm-3" >
            <div class="current-play title" style="padding-bottom:15%">
                Classement
            </div>
            <?php
            if ($found == true) {
                require_once 'artiste-class.php';
            } else {
                print '<div class="show-current dark">';
                print('Cet artiste ne figure pas dans le Top 5000...');
                print '</div>';
            }
            ?>

        </div>
	<div class="full-width container" style=" padding-top: 5%">
		<div class="ct-chart"></div>
		  <script>
              var jsonData = '<?php print GetPlayTime('artiste', $artiste_id)?>';
              var data = JSON.parse(jsonData);
              new Chartist.Line('.ct-chart', data, {divisor: 1, low:0, area:true});
		</script>
	</div>
</div>
</section>
