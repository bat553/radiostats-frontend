<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 23/02/2018
 * Time: 19:25
 */

if ($found == True && $classement > 2) {
    print '<div class="show-old  dark">';
    $infos = GetMorceauID($array_classement[4]);
    print('#' . ($classement - 2) . '<a class="dark-link" href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a> | <a class="dark-link" href="./infodex.php?morceau_id='.$infos['morceau_id'].' " >'.$infos['morceau'].'</a>' );
    print '</div>';
}
if ($found == True && $classement > 1) {
    print '<div class="show-old  dark">';
    $infos = GetMorceauID($array_classement[5]);
    print('#' . ($classement - 1) . '<a class="dark-link" href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a> | <a class="dark-link" href="./infodex.php?morceau_id='.$infos['morceau_id'].' " >'.$infos['morceau'].'</a>');
    print '</div>';
}
if ($found == True && $classement > 0) {
    print '<div class="show-old  dark">';
    $infos = GetMorceauID($array_classement[6]);
    print('#' . ($classement) . '<a class="dark-link" href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a> | <a class="dark-link" href="./infodex.php?morceau_id='.$infos['morceau_id'].' " >'.$infos['morceau'].'</a>');
    print '</div>';
}
if ($found == True) {
    print ('<div class="show-current  dark">');
    $infos = GetMorceauID($array_classement[0]);
    print('#' . ($classement + 1) . '<a class="dark-link" href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a> | <a class="dark-link" href="./infodex.php?morceau_id='.$infos['morceau_id'].' " >'.$infos['morceau'].'</a>');
    print '</div>';
}
if ($found == True && $classement < 4999) {
    print '<div class="show-past  dark">';
    $infos = GetMorceauID($array_classement[3]);
    print('#' . ($classement + 2) . '<a class="dark-link" href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a> | <a class="dark-link" href="./infodex.php?morceau_id='.$infos['morceau_id'].' " >'.$infos['morceau'].'</a>');
    print '</div>';
}
if($found == True && $classement < 4998 ) {
    print '<div class="show-past  dark">';
    $infos = GetMorceauID($array_classement[2]);
    print('#' . ($classement + 3) . '<a class="dark-link" href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a> | <a class="dark-link" href="./infodex.php?morceau_id='.$infos['morceau_id'].' " >'.$infos['morceau'].'</a>');
    print '</div>';
}
if($found == True &&  $classement < 4997 ) {
    print '<div class="show-past  dark">';
    $infos = GetMorceauID($array_classement[1]);
    print('#' . ($classement + 4) . '<a class="dark-link" href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a> | <a class="dark-link" href="./infodex.php?morceau_id='.$infos['morceau_id'].' " >'.$infos['morceau'].'</a>');
    print '</div>';
}
