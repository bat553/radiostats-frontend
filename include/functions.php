<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 21/02/2018
 * Time: 20:30
 */

// SETUP elasticsearch
require '../vendor/autoload.php';
use Elasticsearch\ClientBuilder;

$hosts = [
    'host' => '10.8.0.2',
    'port' => '9200',
    'user' => 'elastic',
    'pass' => 'Swano74370!'
];
$client = ClientBuilder::create()
    ->setHosts($hosts)
    ->build();


// Setup deezer
require_once 'class.deezerapi.php';
$config = array(
    'app_id' => "258142",
    'app_secret' => "e573242dba4746ccf8c6a8c699a836fb",
    'my_url' => "http://swla.be/oauth/deezer.php"
);
$dzapi = new deezerapi($config);

// Setup Spotify
$client_id = '2e3f51b377f14a6da4c2e2eb3fff4fc1';
$client_secret = '3ed4f236d2bc47059e03038a241ae94f';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,            'https://accounts.spotify.com/api/token' );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt($ch, CURLOPT_POST,           1 );
curl_setopt($ch, CURLOPT_POSTFIELDS,     'grant_type=client_credentials' );
curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Authorization: Basic '.base64_encode($client_id.':'.$client_secret)));

$result=curl_exec($ch);
$result = json_decode($result);


$apiKey = $result->access_token; // should match with Server key
$headers = array(
    'Authorization: Bearer '.$apiKey
);

// Setup MySQL

$host = '10.8.0.2';
$db = 'radiostats';
$user = 'swano';
$pass = 'Swano74370!';

$bdd = new PDO('mysql:host='.$host.';dbname='.$db.';charset=utf8', $user, $pass);
array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);



function CurrentPlay()
{
    global $client;
    $params = [
        'index' => 'radio-entry',
        'type' => '_doc',
        'body' => [
            'query' => [
                "match_all" => (object)[]
            ],
            "size" => 2,
            "sort" => [
                "date" => [
                    "order" => "desc"
                ]
            ]
        ]
    ];
    $results = $client->search($params);
    $morceau_id0 = $results['hits']['hits'][0]['_source']['morceau_id'];
    $morceau_id1 = $results['hits']['hits'][1]['_source']['morceau_id'];
    $radio0 = ucwords($results['hits']['hits'][0]['_source']['radio']);
    $radio1 = ucwords($results['hits']['hits'][1]['_source']['radio']);
    $date0 = $results['hits']['hits'][0]['_source']['date'];
    $date1 = $results['hits']['hits'][1]['_source']['date'];

    $infos0 = GetMorceauID($morceau_id0);
    $infos1 = GetMorceauID($morceau_id1);
    return array($infos0, FormatDate($date0), $radio0, $infos1, FormatDate($date1), $radio1);


}

function FormatDate($date)
{
    $date = date_create($date);
    $date = date_format($date, 'H:m:s');
    //$date = date('H:i:s', strtotime($date . ' +1 hour'));
    return $date;
}

function GetArtisteID($artiste_id)
{
    global $client;
    $params = [
        'index' => 'radio-link',
        'body' => [
            'query' => [
                'match' => [
                    'artiste_id' => $artiste_id
                ]
            ]
        ],
    ];
    $results = $client->search($params);
    return $results['hits']['hits'][0]['_source'];
}

function GetMorceauID($morceau_id)
{


    global $client;
    $params = [
        'index' => 'radio-link',
        'body' => [
            'query' => [
                'match' => [
                    'morceau_id' => $morceau_id
                ]
            ]
        ],
    ];
    $results = $client->search($params);
    return $results['hits']['hits'][0]['_source'];
}

function GetRadioInfos($radio)
{
    global $bdd;
    $req = $bdd->prepare("SELECT * FROM radios WHERE name=:radio ORDER BY name");
    $req->execute(array(
        ":radio" => $radio
    ));
    $result = $req->fetchAll();

    return $result;

}

function GetRadioNote($radio){

    global $client;
    $json = '{
    "size":0,
      "aggs": {
        "_doc": {
          "date_range": {
            "field": "date",
             "format": "yyyy-MM-dd HH:mm:ss",
              "ranges": [
                  {"from": "now-1M/M",
                   "to": "now+1h"
                   }
                 ]
           },
          "aggs": {
            "_doc":{
            "terms": {
              "field": "radio",
              "size": 15
            }
          }
          }
        }
      }
    }';
    $params = [
        'index' => 'radio-entry',
        'type' => '_doc',
        'body' => json_decode($json)

    ];
    $results = $client->search($params);
    $results = $results['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];
    $classement = array_search($radio, array_column($results, 'key'));
    $percent = ($results[$classement]['doc_count'] / Get1MonthTotal()) * 100;
    $percent = round($percent);

    return array(GetNote($results, $classement), $percent);


}

function Get1MonthTotal(){
    global $client;
    global $params;
    $json = '{
      "size": 0,
      "aggs": {
        "_doc": {
          "date_range": {
            "field": "date",
            "format": "yyyy-MM-dd HH:mm:ss",
            "ranges": [
              {
                "from": "now-1M/M",
                "to": "now+1h"
              }
            ]
          },
          "aggs": {
            "_doc": {
              "value_count": {
                "field": "morceau_id"
              }
            }
          }
        }
      }
    }';
    $params = [
        'index' => 'radio-entry',
        'type' => '_doc',
        'body' => json_decode($json)

    ];
    $results = $client->search($params);
    return $results['aggregations']['_doc']['buckets'][0]['_doc']['value'];

}


function GetNote($array, $class){

    $end = count($array);
    if($class <= ($end * 0.1)){

        $note = array("A+", "lime");
    }
    elseif($class <= ($end * 0.2)){

        $note = array("A", "#2f892f");
    }
    elseif($class <= ($end * 0.3)){

        $note = array("B+", "#0d740d");
    }
    elseif($class <= ($end * 0.4)){

        $note = array("B", "#849f1a");
    }
    elseif($class <= ($end * 0.5)){

        $note = array("C+", "#bfa519");
    }
    elseif($class <= ($end * 0.6)){

        $note = array("C", "#ffd700");
    }
    elseif($class <= ($end * 0.7)){

        $note = array("D+", "#c84f0e");
    }
    elseif($class <= ($end * 0.8)){

        $note = array("D", "#e41010");
    }
    elseif($class <= ($end * 0.9)){

        $note = array("D-", "red");
    }
    else{

        $note = array("Non noté", "#625a5a");

    }

    return $note;

}

function GetTop15()
{

    global $client;
    $json = '{
    "size":0,
  "aggs": {
    "_doc": {
      "date_range": {
        "field": "date",
         "format": "yyyy-MM-dd HH:mm:ss",
          "ranges": [
              {"from": "now-1M/M",
               "to": "now+1h"
               }
             ]
       },
      "aggs": {
        "_doc":{
        "terms": {
          "field": "morceau_id",
          "size": 15
        }
      }
      }
    }
  }
}';
    $params = [
        'index' => 'radio-entry',
        'type' => '_doc',
        'body' => json_decode($json)

    ];
    $results = $client->search($params);
    return $results['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];

}

function GetTrending()
{
    global $client;
    $json = '{
    "size":0,
      "aggs": {
        "_doc": {
      "date_range": {
        "field": "date",
         "format": "yyyy-MM-dd HH:mm:ss",
          "ranges": [
              {"from": "now-5d/d",
               "to": "now+1h"
               }
             ]
       },
          "aggs": {
            "_doc":{
            "terms": {
              "field": "morceau_id",
              "size": 15,
              "order": {
                "_count": "desc"
                }
            }
          }
          }
        }
      }
    }';
    $params = [
        'index' => 'radio-entry',
        'type' => '_doc',
        'body' => json_decode($json)

    ];
    $results = $client->search($params);
    $all = array(
        $results['aggregations']['_doc']['buckets'][0]['_doc']['buckets'],
        $results['aggregations']['_doc']['buckets'][1]['_doc']['buckets']
    );
    return $all;
}

function GetUpDown($a, $morceau_id, $array)
{

    if ($array) {
        $old = array_search($morceau_id, array_column($array, 'key'));
        if ($old != False) {
            $updown = $a - $old;
        } else {
            $updown = 'new';
        }

        if ($updown > 0) { // positif
            $updown = '<i class="material-icons">&#xE8E3;</i>';
        } elseif ($updown < 0) { // negatif
            $updown = '<i class="material-icons">&#xE8E5;</i>';
        } elseif ($updown == 'new') { // nouveau
            $updown = '<i class="material-icons">&#xE05E;</i>';
        } elseif ($updown == 0) { // pas de progression
            $updown = '<i class="material-icons">&#xE8E4;</i>';
        }
    } else {
        $updown = '<i class="material-icons">&#xE05E;</i>';
    }
    return $updown;

}

function GetTrackDeezer($deezer_id)
{
    global $dzapi;
    $infos = $dzapi->getTrack($deezer_id);
    return $infos;
}

function GetArtistDeezer($deezer_art_id)
{
    global $dzapi;
    $infos = $dzapi->getArtist($deezer_art_id);
    return $infos;
}

function GetTrackSpotify($spotifyid)
{
    global $headers;
    $ch = curl_init("https://api.spotify.com/v1/tracks/$spotifyid");
    // To save response in a variable from server, set headers;
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // Get response
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    return array((array)json_decode($response), $httpcode);
}

function GetArtistSpotify($spotifyid)
{
    global $headers;
    $ch = curl_init("https://api.spotify.com/v1/artists/$spotifyid");
    // To save response in a variable from server, set headers;
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    // Get response
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    return array((array)json_decode($response), $httpcode);
}


function GetTrendingRadio($type, $radio)
{
    global $client;
    if ($type == 'artiste') {
        $json = '{
          "size": 0,
          "query" : {
                "match" : { "radio" : "' . $radio . '" }
            },
          "aggs": {
            "_doc": {
              "date_range": {
                "field": "date",
                 "format": "yyyy-MM-dd HH:mm:ss",
                  "ranges": [
                      {"from": "now-14d/d",
                       "to": "now+1h"
                       }
                     ]
               },
              "aggs": {
                "_doc":{
                "terms": {
                  "field": "artiste_id",
                  "size" : 15
                }
              }
              }
            }
          }
        }';
    }
    else if ($type == 'morceau') {
        $json = '{
          "size": 0,
          "query" : {
                "match" : { "radio" : "' . $radio . '" }
            },
          "aggs": {
            "_doc": {
              "date_range": {
                "field": "date",
                 "format": "yyyy-MM-dd HH:mm:ss",
                  "ranges": [
                      {"from": "now-14d/d",
                       "to": "now+1h"
                       }
                     ]
               },
              "aggs": {
                "_doc":{
                "terms": {
                  "field": "morceau_id",
                  "size" : 15
                }
              }
              }
            }
          }
        }';
    }
    else {
        print('error');
        exit(2);
    }
    $params = [
        'index' => 'radio-entry',
        'type' => '_doc',
        'body' => json_decode($json)

    ];
    $results = $client->search($params);

    return     $results['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];
}

function GetMorceauTop5000()
{
    $url = "http://10.8.0.2:9400/top5000.json";
    // Send request to Server
        $ch = curl_init($url);
    // To save response in a variable from server, set headers;
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "frontend01:Swano74370!");
    // Get response
        $response = curl_exec($ch);
    return $response;
}

function GetArtistTop5000()
{
    $url = "http://10.8.0.2:9400/top5000-art.json";
    // Send request to Server
    $ch = curl_init($url);
    // To save response in a variable from server, set headers;
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERPWD, "frontend01:Swano74370!");
    // Get response
    $response = curl_exec($ch);
    return $response;
}

function GetMorceauRadio($morceau_id)
{
    global $client;
    // + 1 = UTC+1
    $json = '{
              "size": 0,
              "query" : {
                    "match" : { "morceau_id" : "' . $morceau_id . '" }
                },
              "aggs": {
                "_doc": {
                  "date_range": {
                    "field": "date",
                    "format": "yyyy-MM-dd HH:mm:ss",
                    "ranges": [
                      {"from": "now-14d/d",
                        "to": "now+1h"
                      }
                    ]
                  },
                  "aggs": {
                    "_doc":{
                    "terms": {
                      "field": "radio",
                      "size" : 20
                    }
                  }
                  }
                }
              }
            }';
    $params = [
        'index' => 'radio-entry',
        'type' => '_doc',
        'body' => json_decode($json)

    ];
    $results = $client->search($params);
    return $results['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];


}

function GetArtistRadio($artiste_id)
{
    global $client;
    $json = '{
              "size": 0,
              "query" : {
                    "match" : { "artiste_id" : "' . $artiste_id . '" }
                },
              "aggs": {
                "_doc": {
                  "date_range": {
                    "field": "date",
                    "format": "yyyy-MM-dd HH:mm:ss",
                    "ranges": [
                      {"from": "now-14d/d",
                        "to": "now+1h"
                      }
                    ]
                  },
                  "aggs": {
                    "_doc":{
                    "terms": {
                      "field": "radio",
                      "size" : 20
                    }
                  }
                  }
                }
              }
            }';
    $params = [
        'index' => 'radio-entry',
        'type' => '_doc',
        'body' => json_decode($json)

    ];
    $results = $client->search($params);
    return $results['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];


}

function GetArtistTopTracks($artiste_id){
    global $client;
    $json = '{
              "size": 0,
              "query" : {
                    "match" : { "artiste_id" : "' . $artiste_id . '" }
                },
              "aggs": {
                "_doc": {
                  "date_range": {
                    "field": "date",
                    "format": "yyyy-MM-dd HH:mm:ss",
                    "ranges": [
                      {"from": "now-14d/d",
                        "to": "now+1h"
                      }
                    ]
                  },
                  "aggs": {
                    "_doc":{
                    "terms": {
                      "field": "morceau_id",
                      "size" : 20
                    }
                  }
                  }
                }
              }
            }';
    $params = [
        'index' => 'radio-entry',
        'type' => '_doc',
        'body' => json_decode($json)

    ];
    $results = $client->search($params);
    return $results['aggregations']['_doc']['buckets'][0]['_doc']['buckets'];



}


function GetPlayTime($type, $id){
    global $client;
    if ($type == 'artiste') {
        $json = '{
          "size" : 0,
          "aggs": {
            "_doc": {
              "date_histogram": {
                "field": "date",
                "interval": "hour",
                "format": "k"
              }
            }
          },
          "query": {
            "match": {
              "artiste_id": "'.$id.'"
            }
          }
        }';
    }
    else if ($type == 'morceau') {
        $json = '{
          "aggs": {
            "_doc": {
              "date_histogram": {
                "field": "date",
                "interval": "hour",
                "format": "k"
              }
            }
          },
          "query": {
            "match": {
              "morceau_id": "'.$id.'"
            }
          }
        }';
    }
    else if ($type == 'radio') {
        $json = '{
          "aggs": {
            "_doc": {
              "date_histogram": {
                "field": "date",
                "interval": "hour",
                "format": "k"
              }
            }
          },
          "query": {
            "match": {
              "radio": "' . $id . '"
            }
          }
        }';
    }
    $params = [
        'index' => 'radio-entry',
        'type' => '_doc',
        'body' => json_decode($json)

    ];
    $results = $client->search($params);
    $playtime = $results['aggregations']['_doc']['buckets'];
    $array_playtime = array();
    $a =0;
    foreach ($playtime as $row){

        $hour = $row['key_as_string'];
        $count = $row['doc_count'];
        #if ($count == 0) {$count = NULL;}
        $array_playtime['labels'][$a] = $hour;
        $array_playtime['series'][0][$a] = $count;
        ++$a;
    }
    return json_encode($array_playtime);



}

function SetTitle($title){
    print('<title>'.$title.'</title>');
}


