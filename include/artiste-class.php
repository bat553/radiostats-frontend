<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 23/02/2018
 * Time: 19:25
 */

if ($found == True && $classement > 2) {
    print '<div class="show-old  dark">';
    $infos = GetArtisteID($array_classement[4]);
    print('#' . ($classement - 2) . '<a href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a>');
    print '</div>';
}
if ($found == True && $classement > 1) {
    print '<div class="show-old  dark">';
    $infos = GetArtisteID($array_classement[5]);
    print('#' . ($classement - 1) . '<a href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a>');
    print '</div>';
}
if ($found == True && $classement > 0) {
    print '<div class="show-old  dark">';
    $infos = GetArtisteID($array_classement[6]);
    print('#' . ($classement) . '<a href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a>');
    print '</div>';
}
if ($found == True) {
    print ('<div class="show-current  dark">');
    $infos = GetArtisteID($array_classement[0]);
    print('#' . ($classement + 1) . ' ' . $infos['artiste']); // Array commence à 0 donc plus 1 pour avoir un classement qui va de 1 à 100
    print '</div>';
}
if ($found == True && $classement < 4999) {
    print '<div class="show-past  dark">';
    $infos = GetArtisteID($array_classement[3]);
    print('#' . ($classement + 2) . '<a href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a> ');
    print '</div>';
}
if($found == True && $classement < 4998 ) {
    print '<div class="show-past  dark">';
    $infos = GetArtisteID($array_classement[2]);
    print('#' . ($classement + 3) . '<a href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a> ');
    print '</div>';
}
if($found == True &&  $classement < 4997 ) {
    print '<div class="show-past  dark">';
    $infos = GetArtisteID($array_classement[1]);
    print('#' . ($classement + 4) . '<a href="./infodex.php?artiste_id=' . $infos['artiste_id'] . '" > ' . $infos['artiste'] . '</a>');
    print '</div>';
}
