<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 21/02/2018
 * Time: 20:25
 */

$top15 = GetTop15();
$all = GetTrending();
$trending = $all[0];
$trendingminus1 = $all[1];

?>

<div class="container dark full-width col" style="text-align:center; ">
    <div class="row">
        <div class="col-sm-6" style="text-align:center; ">
            <p class="title"> Top 15</p>
            <div style="overflow:scroll; height: 250px; width: 100%; overflow-x: hidden;">
                <table class="table table-dark table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Artiste</th>
                        <th scope="col">Morceau</th>
                        <th scope="col">Joué</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $a = 0;
                    while ($a < count($top15)){
                        $morceau_id = $top15[$a]['key'];
                        $play = $top15[$a]['doc_count'];
                        $infos = GetMorceauID($morceau_id);
                        $artiste = $infos['artiste'];
                        $morceau = $infos['morceau'];
                        $artiste_id = $infos['artiste_id'];
                        $a++;
                        echo "<tr><th>".$a."</th><th><a href='./infodex.php?artiste_id=".$artiste_id."'>". $artiste ."</a></th><th><a href='./infodex.php?morceau_id=".$morceau_id."'>". $morceau ."</a></th><th>". $play ."</th></tr>";}?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-6" style="text-align:center; ">
            <p class="title"> Virals Tracks</p>
            <div style="overflow:scroll; height: 250px; width: 100%; overflow-x: hidden;">
                <table class="table table-dark table-striped" >
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Artiste</th>
                        <th scope="col">Morceau</th>
                        <th scope="col">Trend</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if ($trending != NULL){
                        $a = 0;
                        while ($a < count($trending)){
                            $morceau_id = $trending[$a]['key'];
                            $infos = GetMorceauID($morceau_id);
                            $artiste = $infos['artiste'];
                            $morceau = $infos['morceau'];
                            $artiste_id = $infos['artiste_id'];
                            $updown = GetUpDown($a, $morceau_id, $trendingminus1);
                            ++$a;

                            echo "<tr><th>".$a."</th><th><a href='./infodex.php?artiste_id=".$artiste_id."'>". $artiste ."</a></th><th><a href='./infodex.php?morceau_id=".$morceau_id."'>". $morceau ."</a></th><th>".$updown."</th></tr>";

                        }
                    }

                    else { echo "<tr>Le classement des tendances n'est pas encore opérationnel. Revenez dans quelques heures.</tr>";}
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
