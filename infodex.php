<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 21/02/2018
 * Time: 20:59
 */
require_once 'include/head.php';
if($_GET){

    if(isset($_GET['morceau_id']) && GetMorceauID($_GET['morceau_id'])){
        $morceau_id = htmlspecialchars($_GET['morceau_id']);
        require_once ('include/info-song.php');
    }
    elseif (isset($_GET['artiste_id']) && GetArtisteID($_GET['artiste_id'])){
        $artiste_id = htmlspecialchars($_GET['artiste_id']);
        require_once 'include/info-artiste.php';
    }
    else {
        SetTitle('Erreur 400 | RadioStats v1.2');
        http_response_code(404);
        print("
            <div class='container' style='padding-top: 15%; padding-bottom: 100%'>
                Donnée non trouvée. (404)
            </div>
        ");
    }

}
else{
    SetTitle('Erreur 400 | RadioStats v1.2');
    http_response_code(400);
    print("
    <div class='container' style='padding-top: 15%; padding-bottom: 100%'>
        Pas de données transimsent.
    </div>
    
    ");
}
require_once 'include/footer.php';