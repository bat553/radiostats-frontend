<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 21/02/2018
 * Time: 19:13
 */
require_once('include/head.php');
SetTitle('RadioStats v1.2')
?>


<header class="masthead bg-primary text-white text-center">
    <div class="container">
        <form action="searchdex.php" class="form-inline" method="get">
            <h1 class="text-uppercase mb-0" style="padding-bottom:20px">Lorem ipsum !</h1>
            <input class="rcorners form-control  div-search"  name="q" style="width: 90%" type="search"  placeholder="Search..">
        </form>
    </div>
</header>
<div class="container col" style=" padding-top: 5%">
    <div class="col current-boxes">
        <div class="current-play title" style="padding-bottom:2%">
            Currently playing
        </div>
        <?php include ('include/current-play.php');?>
    </div>
</div>
<section class="bg-primary text-white mb-0" style="margin-top:5%">
<?php require_once ('include/top-array.php')?>
</section>
<?php require_once ('include/footer.php') ?>


