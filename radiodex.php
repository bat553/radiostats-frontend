<?php
/**
 * Created by PhpStorm.
 * User: Swano
 * Date: 23/02/2018
 * Time: 19:49
 */
require_once 'include/head.php';
if ($_GET['radio']) {
    $radio = htmlspecialchars($_GET['radio']);
} else {
    http_response_code(400);
    print("
            <div class='container' style='padding-top: 15%; padding-bottom: 100%'>
                Pas de données valides transimsent.
            </div>
    
        ");
}

$infos = GetRadioInfos($radio)[0];
$title = strtoupper($infos['display_name']). ' | RadioStats v1.2';
SetTitle($title);
if (!$infos['name']){
    http_response_code(404);
    print("
            <div class='container' style='padding-top: 15%; padding-bottom: 100%'>
                Radio non trouvée. (404)
            </div>
    
        ");
}

$trending = GetTrendingRadio('morceau', $radio);
$trendingArt = GetTrendingRadio('artiste', $radio);
$note = GetRadioNote($radio);
//print_r($note);


?>

<body id="page-top">
<header class=" bg-primary text-white text-center">
    <div class="container dark" style='padding-top : 15%; '>
        <div class="row justify-content-md-center"  style="padding-bottom:5%">
            <div class='col col-sm-4 dark'>
                <p class= 'title'><h3><?=strtoupper($infos['display_name'])?></h3></p>
                <p class= 'title'><h5><?=strtoupper($infos['gender'])?></h5></p>
                <br></br>
                <p class= 'title'><h3>Note : </h3> <h2 style="color:<?=$note[0][1]?>"><?=$note[0][0]?> (<?=$note[1]?> % des entrées)</h2></p>

            </div>
            <div class='col col-sm-6 dark'>
                <img src="<?=$infos['picurl']?>" <?=$infos['size']?> alt="<?=$infos['display_name']?>"/>
            </div>
        </div>
    </div>
</header>
<section>
    <div class="container" style="text-align:center; ">
        <div class="row">
            <div class="col-sm-6" style="text-align:center; ">
                <p class="title"> Top artistes</p>
                <div style="overflow:auto; height: 250px; width: 100%; overflow-x: hidden;">
                    <table class="table table-dark table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Artiste</th>
                            <th scope="col">Joué</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        if ($trendingArt != NULL){
                            $a = 0;
                            while ($a < count($trendingArt)){
                                $artiste_id = $trendingArt[$a]['key'];
                                $play = $trendingArt[$a]['doc_count'];
                                $infos = GetArtisteID($artiste_id);
                                $artiste = $infos['artiste'];
                                ++$a;

                                echo "<tr><th>".$a."</th><th><a href='./infodex.php?artiste_id=".$artiste_id."'>". $artiste ."</a></th><th>".$play."</th></tr>";

                            }
                        }

                        else { echo "<tr>Le classement des tendances n'est pas encore opérationnel. Revenez dans quelques heures.</tr>";}
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-sm-6" style="text-align:center; ">
                <p class="title"> Top Tracks</p>
                <div style="overflow:auto; height: 250px; width: 100%; overflow-x: hidden;">
                    <table class="table table-dark table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Morceau</th>
                            <th scope="col">Artiste</th>
                            <th scope="col">Joué</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        if ($trending != NULL){
                            $a = 0;

                            while ($a < count($trending)){
                                $morceau_id = $trending[$a]['key'];
                                $play = $trending[$a]['doc_count'];
                                $infos = GetMorceauID($morceau_id);
                                $artiste = $infos['artiste'];
                                $morceau = $infos['morceau'];
                                $artiste_id = $infos['artiste_id'];
                                ++$a;

                                echo "<tr><th>".$a."</th><th><a href='./infodex.php?artiste_id=".$artiste_id."'>". $artiste ."</a><th><a href='./infodex.php?morceau_id=".$morceau_id."'>". $morceau ."</a></th><th>".$play."</th></tr>";

                            }
                        }

                        else { echo "<tr>Le classement des tendances n'est pas encore opérationnel. Revenez dans quelques heures.</tr>";}
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="full-width container" style=" padding-top: 5%">
            <div class="ct-chart"></div>
            <script>
                var jsonData = '<?php print GetPlayTime('radio', $radio)?>';
                var data = JSON.parse(jsonData);
                new Chartist.Line('.ct-chart', data, {divisor: 1, low:0, area:true});

            </script>
        </div>
    </div>
</section>
